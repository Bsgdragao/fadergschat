package com.example.fadergschat.Fragments;

import com.example.fadergschat.Notifications.MyResponse;
import com.example.fadergschat.Notifications.Sender;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface APIService {
    @Headers(
            {
                    "Content-Type:application/json",
                    "Authorization:key=AAAAvNwgRIU:APA91bGG8mNZ1vlzIcbmFW_ptZcG-F1e2gYs2sjIIM_YwppOl37X6KJhuR8zhKkTkXeUVOL82vOfvA7S-G-fvwuCLuXBL9GGZxoqK7oWgN0yFOCXzPG4AEci7_uBy7sZ4rrqh47CeWBV"
            }
    )

    @POST("fcm/send")
    Call<MyResponse> sendNotification(@Body Sender body);
}